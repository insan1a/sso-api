.PHONY: help
help: ## Show this help.
	@sed -ne '/@sed/!s/## //p' $(MAKEFILE_LIST)

.PHONY: service/build
service/build:
	@go build -o ./bin/sso-api ./cmd/sso-api

.PHONY: service/run
service/run: service/build
	@./bin/sso-api

.PHONY: service/test
service/test:
	@go test -v ./...

.PHONY: service/cover
service/cover:
	@go test -v --coverprofile=cover.out ./... \
	&& go tool cover -html=cover.out \
	&& rm cover.out

.PHONY: docker/build
docker/build: ## Build the Docker image
	@docker build . -t ak1m1tsu/sso-api:${SSO_API_CONTAINER_VERSION}

.PHONY: docker/push
docker/push:
	@docker push ak1m1tsu/sso-api:${SSO_API_CONTAINER_VERSION}

.PHONY: docker/up
docker/up: ## Run the Docker Compose services
	@docker compose up -d

.PHONY: docker/down
docker/down: ## Stop the Docker Compose services
	@docker compose down

.PHONY: docker/clean
docker/clean: ## Remove the built Docker image
	@docker image rm ak1m1tsu/sso-api:${SSO_API_CONTAINER_VERSION}

.PHONY: proto/gen
proto/gen: ## Generate gRPC Server and Client from Proto file
	@protoc -I api ./api/proto/sso.proto \
		--go_out=. --go_opt=paths=source_relative \
        --go-grpc_out=. --go-grpc_opt=paths=source_relative
