FROM golang:alpine AS builder

WORKDIR /build

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w" -trimpath -o sso-api ./cmd/sso-api

FROM scratch

USER 1000:1000

COPY --from=builder /build/sso-api /
COPY --from=builder /build/config/config.yaml /config/config.yaml

ENV PORT=3000

EXPOSE $PORT

ENTRYPOINT [ "/sso-api" ]
